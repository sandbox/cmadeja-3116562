<?php

namespace Drupal\config_backend_updated\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ConfigBackendUpdated.
 *
 * @package Drupal\config_backend_updated\Services
 */
class ConfigBackendUpdated {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * Config factory.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Logger Interface.
   *
   * @var Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Mail manager interface.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mail;

  /**
   * ConfigBackendUpdated constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger interface.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail
   *   Mail manager interface.
   */
  public function __construct(ConfigFactory $config, LoggerInterface $logger, MailManagerInterface $mail) {
    $this->config = $config;
    $this->logger = $logger;
    $this->mail = $mail;
  }

  /**
   * SendMail method.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   User account object.
   */
  public function sendMail(AccountProxyInterface $account) {
    $sitename = $this->config->get('system.site')->get('name');
    $langcode = $this->config->get('system.site')->get('langcode');
    $module = 'config_backend_updated';
    $key = 'config_backend_updated';
    $to = $account->getEmail();
    $reply = NULL;
    $send = TRUE;

    $params['message'] = $this->t('Your wonderful message about @sitename', ['@sitename' => $sitename]);
    $params['subject'] = $this->t('Message subject');
    $params['options']['username'] = $account->getAccountName();
    $params['options']['title'] = $this->t('Your wonderful title');
    $params['options']['footer'] = $this->t('Your wonderful footer');

    $this->mail->mail($module, $key, $to, $langcode, $params, $reply, $send);
  }

  /**
   * Store in log method.
   *
   * @param string $configName
   *   The config name.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   User account object.
   */
  public function storeLog($configName, AccountProxyInterface $account) {
    $this->logger->info('The config "@config_name" has been updated by the user "@user_name"', [
      '@config_name' => $configName,
      '@user_name' => $account->getAccountName(),
    ]);
  }

}
