<?php

namespace Drupal\config_backend_updated\EventSubscriber;

use Drupal\config_backend_updated\Services\ConfigBackendUpdated;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A subscriber recording the updated configuration by a user.
 */
class ConfigEventsSubscriber implements EventSubscriberInterface {

  const CONFIG_NAME = 'config_backend_updated.settings';

  /**
   * Admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Config backend updated service.
   *
   * @var \Drupal\config_backend_updated\Services\ConfigBackendUpdated
   */
  protected $configBackendUpdated;

  /**
   * ConfigEventsSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   Current user object.
   * @param \Drupal\config_backend_updated\Services\ConfigBackendUpdated $configBackendUpdated
   *   Config backend updated service.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   Admin context.
   */
  public function __construct(ConfigFactory $config, AccountProxyInterface $account, ConfigBackendUpdated $configBackendUpdated, AdminContext $admin_context) {
    $this->config = $config->get(self::CONFIG_NAME);
    $this->account = $account;
    $this->configBackendUpdated = $configBackendUpdated;
    $this->adminContext = $admin_context;
  }

  /**
   * Record the updated configuration.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Event to process.
   */
  public function onChange(ConfigCrudEvent $event) {
    if ($this->adminContext->isAdminRoute()) {
      if (!empty($this->config->get('email'))) {
        $this->configBackendUpdated->sendMail($this->account);
      }

      if (!empty($this->config->get('syslog'))) {
        $this->configBackendUpdated->storeLog(
          $event->getConfig()->getName(),
          $this->account
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onChange'];
    $events[ConfigEvents::DELETE][] = ['onChange'];

    return $events;
  }

}
