<?php

namespace Drupal\config_backend_updated\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a setting UI for Config backend updated.
 *
 * @package Drupal\config_backend_updated\Form
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_backend_updated.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_backend_updated_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('config_backend_updated.settings');

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Email',
      '#description' => $this->t('The email addresses'),
      '#default_value' => $config->get('email') ?? '',
    ];

    $form['syslog'] = [
      '#type' => 'checkbox',
      '#title' => 'Store in syslog',
      '#description' => $this->t('Log the configuration informations in syslog'),
      '#default_value' => $config->get('syslog') ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config_backend_updated_settings = $this->config('config_backend_updated.settings');
    $config_backend_updated_settings->set('email', $values['email'] ?? '');
    $config_backend_updated_settings->set('syslog', $values['syslog'] ?? '');
    $config_backend_updated_settings->save();
    parent::submitForm($form, $form_state);

  }

}
