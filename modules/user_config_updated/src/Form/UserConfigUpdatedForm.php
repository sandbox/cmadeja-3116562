<?php

namespace Drupal\user_config_updated\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class UserConfigUpdatedForm extends EntityForm {

  /**
   * Constructs an UserConfigUpdatedForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $example = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $example->label(),
      '#description' => $this->t("Label for the Example."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $example->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$example->isNew(),
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $user_config_updated = $this->entity;
    $status = $user_config_updated->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label user_config_updated.', [
        '%label' => $user_config_updated->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label user_config_updated was not saved.', [
        '%label' => $user_config_updated->label(),
      ]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.user_config_updated.collection');
  }

  /**
   * Helper function to check whether an user_config_updated entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('user_config_updated')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
