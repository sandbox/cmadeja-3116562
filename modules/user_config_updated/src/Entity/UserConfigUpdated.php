<?php

namespace Drupal\user_config_updated\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\user_config_updated\UserConfigUpdatedInterface;

/**
 * Defines the user_config_updated entity.
 *
 * @ConfigEntityType(
 *   id = "config_updated",
 *   label = @Translation("User config updated"),
 *   handlers = {
 *     "list_builder" = "Drupal\user_config_updated\Controller\UserConfigUpdatedViewsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\user_config_updated\Form\UserConfigUpdatedForm",
 *       "edit" = "Drupal\user_config_updated\Form\UserConfigUpdatedForm",
 *       "delete" = "Drupal\user_config_updated\Form\UserConfigUpdatedDeleteForm",
 *     }
 *   },
 *   config_prefix = "user_config_updated",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "user_id",
 *     "label"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/user_config_updated/{user_config_updated}",
 *     "delete-form" = "/admin/config/system/user_config_updated/{user_config_updated}/delete",
 *   }
 * )
 */
class UserConfigUpdated extends ConfigEntityBase implements UserConfigUpdatedInterface {

  /**
   * The user config updated ID.
   *
   * @var string
   */
  public $id;

  /**
   * The user ID.
   *
   * @var string
   */
  public $user_id;

  /**
   * The config label.
   *
   * @var string
   */
  public $label;

  // Your specific configuration property get/set methods go here,
  // implementing the interface.
}
