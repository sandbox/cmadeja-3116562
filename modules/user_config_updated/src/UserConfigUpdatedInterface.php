<?php

namespace Drupal\user_config_updated;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Example entity.
 */
interface UserConfigUpdatedInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
